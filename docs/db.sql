-- 用户
CREATE TABLE `User` (
  `user_id`       int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键',
  `openId`        varchar(32)   DEFAULT NULL  COMMENT '微信标识',
  `user_type`     varchar(16)   DEFAULT NULL  COMMENT '用户类型',
  `real_name`     varchar(16)   DEFAULT NULL  COMMENT '真实姓名',
  `nick_name`     varchar(16)   DEFAULT NULL  COMMENT '昵称',
  `mobile_number` varchar(16)   DEFAULT NULL  COMMENT '手机',
  `gmt_modified`  timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='用户';
-- 用户-班级
CREATE TABLE `UserClass` (
  `openId`        varchar(32)   DEFAULT NULL  COMMENT '微信标识',
  `class_id`      int(11)       DEFAULT NULL  COMMENT '主键'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='用户-班级 关系';
-- 班级
CREATE TABLE `Class` (
  `class_id`      int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键',
  `class_code`    varchar(16)   NOT NULL      COMMENT '班级编号',
  `class_name`    varchar(16)   DEFAULT NULL  COMMENT '班级名称',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='班级';
-- 试卷
CREATE TABLE `Subject` (
  `subject_id`    int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_code`  varchar(16)   NOT NULL      COMMENT '科目名',
  `subject_memo`  varchar(16)   DEFAULT NULL  COMMENT '备注',
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='试卷表';
-- 题目
CREATE TABLE `Topic` (
  `topic_id`      int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_id`    int(11)       NOT NULL      COMMENT '科目ID',
  `topic_code`    varchar(16)   NOT NULL      COMMENT '题目ID',
  `topic_type`    varchar(16)   NOT NULL      COMMENT '题目类型radio checkbox',
  `topic_answer`  varchar(16)   NOT NULL      COMMENT '参考答案',
  `topic_title`   varchar(255)  NOT NULL      COMMENT '题目内容',
  `answer_a`      varchar(64)   DEFAULT NULL  COMMENT '答案A',
  `answer_b`      varchar(64)   DEFAULT NULL  COMMENT '答案B',
  `answer_c`      varchar(64)   DEFAULT NULL  COMMENT '答案C',
  `answer_d`      varchar(64)   DEFAULT NULL  COMMENT '答案D',
  `answer_e`      varchar(64)   DEFAULT NULL  COMMENT '答案E',
  `answer_f`      varchar(64)   DEFAULT NULL  COMMENT '答案F',
  `answer_g`      varchar(64)   DEFAULT NULL  COMMENT '答案G',
  `gmt_create`    timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified`  timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted`    tinyint(4)    NOT NULL DEFAULT '1' COMMENT '删除标识 0=有效 1=删除 ',
  PRIMARY KEY (`topic_id`),
  KEY `Index_subject_id_code` (`subject_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='题目表';