let mysql = require('mysql');
let pool = mysql.createPool({
    host     : '47.96.159.157',
    port:3306,
    protocol:'mysql',
    user:'root',
    password : 'jN+100][',
          database : 'ksAppDB',
    connectionLimit:100 //最大连接数
});

let query = function( sql, values ) {
  // 返回一个 Promise
  return new Promise(( resolve, reject ) => {
    pool.getConnection(function(err, connection) {
      if (err) {
        reject( err )
      } else {
        connection.query(sql, values, ( err, rows) => {
          if ( err ) {
            reject( err )
          } else {
            resolve( rows )
          }
          // 结束会话
          connection.release()
        })
      }
    })
  })
}
//module.exports = pool;
module.exports =  query;


