var express = require('express');
var router = express.Router();
var pool=  require('./pool');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('TOPIC');
});

/* GET users listing. */
router.post('/getTopicInfo', function(req, res, next) {
  var obj={   
    "result":"success",                  //有东西，初始化日历入口//false就不显示任何操作，不初始化按钮。
    "count":1,                      //1一个班级,或2两个班。
    "data":[
        {
            "ClassName":"汽配1班",         //日历上面显示的班级名
            "isStudent":true,               //显示查阅题库等操作
            "SubjectID":12341,              //题库编号……
            "StudyPlan1":{                  //学习期：false表示未学习，true表示已经学习
                "2018-10-12":false,
                "2018-10-13":false,
                "2018-10-14":true,
                "2018-10-15":false,
                "2018-10-16":false
            },
            "StudyPlan2":{                  //模拟期：false表示未模拟，true表示已模拟
                "2018-10-17":false,
                "2018-10-18":false
            },              
        },
        {
            "ClassName":"汽配2班",         //日历上面显示的班级名
            "isStudent":true,               //显示查阅题库等操作
            "SubjectID":12342,              //题库编号……
            "StudyPlan2":{                  //学习期：false表示未学习，true表示已经学习
                "2018-10-12":false,         //学习期的日期对象false显示灰，true显示绿
                "2018-10-13":false,
                "2018-10-14":true,
                "2018-10-15":true,
                "2018-10-16":false
            },
            "StudyPlan1":{                  //模拟期：false表示未模拟，true表示已模拟考
                "2018-10-17":false,         //模拟期的日期对象false显示灰，true显示绿
                "2018-10-18":false
            },              
        }
    ]   
};
    res.set('Content-Type','application/json');
    res.send(JSON.stringify(obj));
});

/* GET users listing. */
router.post('/getTopicNormal', function(req, res, next) {
      var obj={   
        "result":"success",
        "attribute":{
            "ClassName":"汽配1班",
            "SubjectID":12341
        },
        "count":0,
        "data":null
      };
//

        var mysql      = require('mysql');
        var connection = mysql.createConnection({
          host     : '47.96.159.157',
          user     : 'root',
          password : 'jN+100][',
          database : 'ksAppDB',
          port:'3306'
        });

        connection.connect();

        connection.query('SELECT * FROM Topic LIMIT 3;', function (error, results, fields) {
          var items=[];
          for(var i=0;i<results.length;i++){
              var item = {};
              item.subjectID = results[i].subject_id;
              item.topicCode = results[i].topic_code;
              item.topicType = results[i].topic_type;
              item.topicAnswer = results[i].topic_answer;
              item.topicTitle = results[i].topic_title;
              item.answerA = results[i].answer_a;
              item.answerB = results[i].answer_b;
              item.answerC = results[i].answer_c;
              item.answerD = results[i].answer_d;
              item.answerE = results[i].answer_e;
              item.answerF = results[i].answer_f;
              item.answerG = results[i].answer_g;
              items.push(item);
            }//end for
          //
          obj.count = results.length;
          obj.data = items;
          res.set('Content-Type','application/json');
          res.send(JSON.stringify(obj));
        });//end connection.query
// 

});

/* GET users listing. */
router.post('/getExam', function(req, res, next) {
      var obj={   
        "result":"success",
        "attribute":{
            "ClassName":"汽配1班",
            "SubjectID":12341
        },
        "count":0,
        "data":null
      };
//

        var mysql      = require('mysql');
        var connection = mysql.createConnection({
          host     : '47.96.159.157',
          user     : 'root',
          password : 'jN+100][',
          database : 'ksAppDB',
          port:'3306'
        });

        connection.connect();

        connection.query('SELECT * FROM Topic LIMIT 3;', function (error, results, fields) {
          var items=[];
          for(var i=0;i<results.length;i++){
              var item = {};
              item.subjectID = results[i].subject_id;
              item.topicCode = results[i].topic_code;
              item.topicType = results[i].topic_type;
              item.topicAnswer = results[i].topic_answer;
              item.topicTitle = results[i].topic_title;
              item.answerA = results[i].answer_a;
              item.answerB = results[i].answer_b;
              item.answerC = results[i].answer_c;
              item.answerD = results[i].answer_d;
              item.answerE = results[i].answer_e;
              item.answerF = results[i].answer_f;
              item.answerG = results[i].answer_g;
              items.push(item);
            }//end for
          //
          obj.count = results.length;
          obj.data = items;
          res.set('Content-Type','application/json');
          res.send(JSON.stringify(obj));
        });//end connection.query
// 

});


/* 生成试卷 */
router.post('/generateExam', function(req, res, next) {
    var obj={   
        "result":"success",
        "attribute":{
            "ClassName":"汽配1班",
            "SubjectID":12341
        },
        "count":0,
        "data":null
    };
    pool.getConnection(function(err,conn){
        if(err){
            //do something
        }//end if
        var sql = 'SELECT * FROM Topic LIMIT 3;';
        var args={};
        conn.query(sql,args,function(err,results){
            if(err){
                 //do something
            }else{
                var items=[];
                for(var i=0;i<results.length;i++){
                    var item = {};
                    item.subjectID = results[i].subject_id;
                    item.topicCode = results[i].topic_code;
                    item.topicType = results[i].topic_type;
                    item.topicAnswer = results[i].topic_answer;
                    item.topicTitle = results[i].topic_title;
                    item.answerA = results[i].answer_a;
                    item.answerB = results[i].answer_b;
                    item.answerC = results[i].answer_c;
                    item.answerD = results[i].answer_d;
                    item.answerE = results[i].answer_e;
                    item.answerF = results[i].answer_f;
                    item.answerG = results[i].answer_g;
                    items.push(item);
                }//end for
                obj.count = results.length;
                obj.data = items;
                res.set('Content-Type','application/json');
                res.send(JSON.stringify(obj));
            }//end if
            conn.release(); //释放连接
        });//end query
    });//end pool
});//end post function


module.exports = router;
