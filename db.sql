-- 用户
CREATE TABLE `User` (
  `user_id` INT (11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `openId` VARCHAR (32) DEFAULT NULL COMMENT '微信标识',
  `user_type` VARCHAR (16) DEFAULT NULL COMMENT '用户类型',
  `real_name` VARCHAR (16) DEFAULT NULL COMMENT '真实姓名',
  `nick_name` VARCHAR (16) DEFAULT NULL COMMENT '昵称',
  `mobile_number` VARCHAR (16) DEFAULT NULL COMMENT '手机',
  `gmt_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`user_id`)
) ENGINE = INNODB AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8 COMMENT = '用户';
-- 用户-班级
CREATE TABLE `UserClass` (
  `openId` VARCHAR (32) DEFAULT NULL COMMENT '微信标识',
  `class_id` INT (11) DEFAULT NULL COMMENT '主键'
) ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT = '用户-班级 关系';
-- 班级
CREATE TABLE `Class` (
  `class_id` INT (11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `class_code` VARCHAR (16) NOT NULL COMMENT '班级编号',
  `class_name` VARCHAR (16) DEFAULT NULL COMMENT '班级名称',
  `subject_id` INT (11) DEFAULT NULL COMMENT '科目',
  `node_start` datetime DEFAULT NULL COMMENT '开始时期',
  `node_split` datetime DEFAULT NULL COMMENT '切分日期',
  `node_end` datetime DEFAULT NULL COMMENT '截止日期',
  PRIMARY KEY (`class_id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT = '班级';
--
--
-- 科目
CREATE TABLE `Subject` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_name` varchar(16) NOT NULL COMMENT '科目名称',
  `subject_longtime` int(11) NOT NULL COMMENT '科目考试时长',  
  `subject_memo` varchar(16) DEFAULT NULL COMMENT '科目备注',
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='科目表';
-- 题目
CREATE TABLE `Topic` (
  `topic_id`      int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_id`    int(11)       NOT NULL      COMMENT '科目ID',
  `topic_code`    varchar(16)   NOT NULL      COMMENT '题目ID',
  `topic_type`    varchar(16)   NOT NULL      COMMENT '题目类型radio checkbox',
  `topic_answer`  varchar(16)   NOT NULL      COMMENT '参考答案',
  `topic_title`   varchar(255)  NOT NULL      COMMENT '题目内容',
  `answer_a`      varchar(64)   DEFAULT NULL  COMMENT '答案A',
  `answer_b`      varchar(64)   DEFAULT NULL  COMMENT '答案B',
  `answer_c`      varchar(64)   DEFAULT NULL  COMMENT '答案C',
  `answer_d`      varchar(64)   DEFAULT NULL  COMMENT '答案D',
  `answer_e`      varchar(64)   DEFAULT NULL  COMMENT '答案E',
  `answer_f`      varchar(64)   DEFAULT NULL  COMMENT '答案F',
  `answer_g`      varchar(64)   DEFAULT NULL  COMMENT '答案G',
  `gmt_create`    timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified`  timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted`    tinyint(4)    NOT NULL DEFAULT '1' COMMENT '删除标识 0=有效 1=删除 ',
  PRIMARY KEY (`topic_id`),
  KEY `Index_subject_id_code` (`subject_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='题目表';

-- 习题题目
CREATE TABLE `UserTopic` (
  `openId` VARCHAR (32) NOT NULL COMMENT '微信标识',
  `subject_id` INT (11) NOT NULL COMMENT '科目ID',
  `topic_id` INT (11) DEFAULT NULL COMMENT '题目主键',
  `use_data` VARCHAR (10) NOT NULL COMMENT '练习时间',
  `is_marker` TINYINT (1) DEFAULT 0 COMMENT '是否收藏',
  `is_learn` TINYINT (1) DEFAULT 0 COMMENT '是否练习',
  `is_err_marker` TINYINT (1) DEFAULT 0 COMMENT '错题标记',
  PRIMARY KEY (`openId`),
  KEY `Index_useData_id_code` (`use_data`) USING BTREE
) ENGINE = INNODB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8 COMMENT = '习题题目';
-- 考试题目
CREATE TABLE `UserExam` (
  `openId` VARCHAR (32) NOT NULL COMMENT '微信标识',
  `subject_id` int(11) NOT NULL COMMENT '科目ID',
  `topic_id` INT (11) DEFAULT NULL COMMENT '题目主键',
  `use_data` VARCHAR (10) NOT NULL COMMENT '考试时间',
  `is_exam` TINYINT (1) DEFAULT 0 COMMENT '是否练习',
  PRIMARY KEY (`openId`),
  KEY `Index_useData_id_code` (`use_data`) USING BTREE
) ENGINE = INNODB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8 COMMENT = '考试题目';
-- 
-- 考试成绩
CREATE TABLE `ExamScore` (
  `openId` VARCHAR (32) NOT NULL COMMENT '微信标识',
  `score` int(11) NOT NULL COMMENT '分数',
  `topic_id` INT (11) DEFAULT NULL COMMENT '题目主键',
  `topic_answer` VARCHAR (10) NOT NULL COMMENT '考试时间时间',
  PRIMARY KEY (`openId`)
) ENGINE = INNODB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8 COMMENT = '考试成绩';